//
// Defines.cs
// Get the locale directory
//
// Authors:
//	Martin Willemoes Hansen
//
// (C) 2004 Martin Willemoes Hansen
//

namespace FSpot.Core {
	public struct Defines {
		public const string LOCALE_DIR = "/home/ruben/Build/share/locale";
		public const string VERSION = "0.8.2";
		public const string PACKAGE = "f-spot";
		public const string PREFIX = "/home/ruben/Build";
		public const string APP_DATA_DIR = "/home/ruben/Build/share/f-spot";
		public const string BINDIR = PREFIX + "/bin";
	}
}
