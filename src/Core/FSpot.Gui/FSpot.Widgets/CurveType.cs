//
// FSpot.Widgets.CurveType.cs
//
// Author(s):
//	Stephane Delcroix  <stephane@delcroix.org>
//
// Copyright (c) 2009 Novell, Inc.
//
// This is open source software. See COPYING for details.
//
// Ported from Gtk+, where this widget and is no longer really supported.
//

namespace FSpot.Widgets
{
	public enum CurveType {
		Linear,
		Spline,
		Free,
	}
}


