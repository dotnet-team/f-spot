//
// FSpot.Widgets.PointerMode.cs
//
// Author(s)
//	Stephane Delcroix  <stephane@delcroix.org>
//
// This is free software. See COPYING for details.
//

namespace FSpot.Widgets
{
	public enum PointerMode {
		None,
		Select,
		Scroll
	}
}
