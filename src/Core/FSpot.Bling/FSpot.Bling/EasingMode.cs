//
// FSpot.Bling.EasingMode.cs
//
// Author(s):
//	Stephane Delcroix  <stephane@delcroix.org>
//
// This is free software. See COPYING for details
//

namespace FSpot.Bling
{
	public enum EasingMode {
		EaseIn = 0,
		EaseOut,
		EaseInOut,
	}
}

